
## 滚动通告栏

### 介绍

- 基于uni-app的  **swiper** 和 **swiper-item** 开发;
- 组件需要依赖 sass 插件 ，请自行安装；
- 可自定义公告栏配置，如同时显示的公告数量，是否自动轮播，轮播间隔等；
- 欢迎大家下载使用，项目源码示例：[https://gitee.com/SuperLucifer/lucifer-notice-bar.git](https://gitee.com/SuperLucifer/lucifer-notice-bar.git)

### API
### 属性说明
| 属性名							  | 类型		|  默认值	|  可选值			| 说明																				|
|---------------------|---------|---------|-------------|---------------------------------------------|
| list								| Array		| []			| -						| 公告数据																		  |
| boxTitle						| String	| 通知公告| -						| 盒子标题																		  |
| titleType						| Number	| 1				| 2						| 盒子标题样式类型，目前提供了两种，可自行拓展  |
| margin							| String	| auto		| -						| 盒子外边距																	  |
| background					| String	| #F2F4F6	| -						| 盒子背景色																	  |
| borderRadius				| String	| 12rpx	  | -						| 盒子圆角																	    |
| displayMultipleItems| Number	| 2				| -						| 同时显示的公告数量													  |
| interval						| Number	| 3000		| -						| 公告轮播时间间隔														  |
| autoplay						| Boolean	| true		| false				| 是否自动轮播																  |
| disableTouch				| Boolean	| true		| false				| 是否禁止用户 touch 操作											|
| showMore						| Boolean	| false		| true				| 显示"更多"按钮															  |
| keyField						| String	| id			| -						| key取值字段																	|
| labelField					| String	| label		| -						| 展示内容取值字段														  |

注意：
1. `boxTitle`建议取四个字，超过四个字可能会影响美观；
2. 微信小程序下，`disableTouch`属性无效；

#### 事件说明
| 事件名				  | 说明								| 返回值																																  |
|--------------	|---------------------|-----------------------------------------------------------------------|
| @notice-click	| 点击公告时触发			  | 返回值e={data(Object),index(Number)}，data：公告数据；index：公告下标	  |
| @more-click		| 点击'更多'按钮时触发 | -																																			|

### 基本用法
在`template`中使用组件
```
<lucifer-notice-bar box-title="通知公告" :list="noticeList" margin="20rpx" 
	label-field="noticeTitle" show-more @notice-click="onNoticeClick" @more-click="onMoreClick">
</lucifer-notice-bar>
```

在`script`中使用
```
<script>
export default {
	data() {
		return {
			noticeList: [
				{ id: 1, noticeTitle: "疑发动机起火，一架从成都起飞的航班紧急降落" }, 
				{ id: 2, noticeTitle: "降温降雨来了！个别乡镇或有大暴雨" }, 
				{ id: 3, noticeTitle: "全城致敬！今晚重庆地标建筑将为教师“亮灯”" }, 
				{ id: 4, noticeTitle: "保护好、传承好、弘扬好长江文化，长江文明论坛9月12日在重庆举行" }, 
			],
		};
	},
	methods: {
		// 点击公告
		onNoticeClick(event) {
			console.log('点击公告: ', event)
			uni.showToast({
				title: `点击公告：${event.data.noticeTitle}`,
				icon: 'none'
			})
		},
		// 点击更多
		onMoreClick(){
			console.log('点击更多')
			uni.showToast({
				title: '点击更多',
				icon: 'none'
			})
		}
	}
};
</script>
```

### 说明
1. 亲测H5端、APP端、微信小程序可用，其他小程序暂时还没测过；
2. 有任何问题或想法可以直接留言评论，或添加作者QQ：1768674705，记得备注哦。
