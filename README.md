# lucifer-notice-bar 组件项目示例

#### 介绍
基于uni-app的swiper和swiper-item开发的滚动通告栏组件项目示例
- 组件说明文档详见/uni_modules/lucifer-notice-bar/readme.md

#### 安装教程

1.  克隆项目 git clone [https://gitee.com/SuperLucifer/lucifer-notice-bar.git](https://gitee.com/SuperLucifer/lucifer-notice-bar.git)
2.  使用Hbuilder X 打开项目
